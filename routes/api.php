<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('students', 'App\Http\Controllers\ProfileController@index');
Route::get('student/{id}', 'App\Http\Controllers\ProfileController@show');
Route::post('student-create', 'App\Http\Controllers\ProfileController@store');
Route::put('student-update', 'App\Http\Controllers\ProfileController@update');
Route::delete('student-delete/{id}', 'App\Http\Controllers\ProfileController@destroy');
