<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;

class ProfileController extends Controller{


    public function index(Profile $profile){
        return $profile->all();
    }

    public function show(Profile $profile, $id){
        return $profile->where('id', $id)->first();
    }

    public function store(Request $request, Profile $profile){
        return $profile->create($request->all());
    }

    public function update(Request $request, Profile $profile){
        return $profile->where('id', $request->id)->update($request->all());
    }

    public function destroy(Profile $profile, $id){
        return $profile->where('id', $id)->delete();
    }
}
